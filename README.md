# Etermax ML challenge

#### Resumen

Se utilizó un modelo de árboles de regresión a través de Gradient Boosting, implementado a través de la librería LightGBM, para estimar el revenue a partir de las categorías `event_2`, `event_3`, `platform` y `source` del dataset entregado. Se realizó un preprocesamiento de los datos con una transformación al espacio logarítimico del revenue debido a los outliers correspondientes a _whales_.  Se utilizó MLFlow para realizar los experimentos de entrenamiento, el logging y trazability, y servir el modelo. Se escribió un microservicio en Flask que expone un endpoint, que adquiere a través de un JSON los campos del usuario a estimar, estima el valor de revenue y guarda dicha información en una base de datos. Tanto el microservicio como la base de datos fueron containerizados en dos Dockerfiles distintos. Se escribieron tests con Pytest para asegurar el correcto funcionamiento del microservicio.

## Instrucciones de uso

### Modelo

Es fundamental correr el servidor de MLFlow antes de utilizar el modelo. Se puede utilizar un servidor de MLFlow remoto o un servidor local. Si se desea utilizar un servidor local, se debe correr:

```sh
$ export MLFLOW_TRACKING_URI=http://localhost:5050
$ mlflow server --backend-store-uri  sqlite:///mlflow.db  --default-artifact-root ./artifacts --host 127.0.0.1 --port 5050 
```

Luego, se puede entrenar y generar el modelo -desde la raíz del proyecto- utilizando el siguiente comando:

```sh
$ mlflow run models -P parameters=values
```
donde parameters:
- --learning_rate: {tipo: float, default: 0.005}
- --max_depth: {tipo: int, default: 8}
- --num_leaves: {tipo: int, default: 128}
- --num_iterations: {tipo: int, default: 100000}
- --n_estimators: {tipo: int, default: 1000}

Una vez creado el modelo, el mismo puede ser "servido" a través de la siguiente manera: Se debe ingresar a la interfaz gráfica del servidor MLFlow (con un navegador, a través de localhost:5050) y buscar la última versión del modelo entrenado. Luego, se copia la dirección donde se encuentra y se sirve a tráves de MLFlow. En mi caso, este comando toma la forma:

```sh
$ mlflow models serve -m models/artifacts/0/393ac124b8154e38a71e269688a3c846/artifacts/model -p 1234
```

De esta forma el modelo ya se encuentra escuchando en localhost:1234 los requests que le llegan. Estos deben tener la forma de JSON y además estar en un pandas dataframe.

### Microservicio

Para correr la aplicación del microservicio y la base de datos se deben construir las imágenes que les corresponden. 

Primero, levantamos un docker con MySQL - siendo escuchado en el puerto 3306:

```sh
    docker run mysql:latest -p 3306:3306
```




