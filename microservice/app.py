'''
Simple microservice to infere the revenue on a new user.
=======================================================

'''

from flask import Flask, jsonify, make_response, request
from flask_json_schema import JsonSchema, JsonValidationError
from schema.schema import row_schema
from utils import preprocessor, logger


integers = ["event_2", "event_3"]
categoricals = ["platform", "source"]


app = Flask(__name__)
schema = JsonSchema(app)

@app.errorhandler(JsonValidationError)
def validation_error(e):
    return jsonify({ 'error': e.message, 'errors': [validation_error.message for validation_error  in e.errors]})

@app.route('/')
def welcome():
    print("at least im here")
    return make_response(jsonify({"working": True}))

@app.route('/infere', methods=['POST'])
@schema.validate(row_schema)
def infere():
    x = preprocess(request.json, categoricals, integers)
    y = anti_transform(model.predict(x))

    return make_response(jsonify({'revenue': y}))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)