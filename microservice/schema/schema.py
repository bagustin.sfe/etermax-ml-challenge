
row_schema = {
    'required': ['event_2', 'event_3', 'source', 'platform'],
    'properties': {
        'event_2': {'type': 'integer'},
        'event_3': {'type': 'integer'},
        'source': {'type': 'string'},
        'platform': {'type': 'string'}
    }
}