def preprocessor(json_request, categories, integers):
    preprocessed = {}
    for cat in categories:
        preprocessed[cat] = json_request[cat].lower()
    for integer in integers:
        preprocessed[cat] = json_request[cat].lower()

    preprocessed = {
        "event_2": int(json_request["event_2"]),
        "event_3": int(json_request["event_3"]),
        "source": json_request["source"].lower(),
        "platform": json_request["platform"].lower()
    }
    return preprocessed