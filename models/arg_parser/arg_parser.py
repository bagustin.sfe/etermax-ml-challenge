import argparse

def parse_args():
    parser = argparse.ArgumentParser(description="Model training and deployment for Etermax challenge")
    parser.add_argument(
        "--learning-rate",
        type=float,
        default=0.05,
        help="learning rate to update step size at each boosting step (default: 0.05)",
    )
    parser.add_argument(
        "--max-depth",
        type=int,
        default=8,
        help="subsample ratio of columns when constructing each tree (default: 1.0)",
    )
    parser.add_argument(
        "--num-leaves",
        type=int,
        default=128,
        help="subsample ratio of the training instances (default: 1.0)",
    )
    parser.add_argument(
        "--num-iterations",
        type=int,
        default=10000,
        help="subsample ratio of the training instances (default: 1.0)",
    )
    parser.add_argument(
        "--n-estimators",
        type=int,
        default=1000,
        help="subsample ratio of the training instances (default: 1.0)",
    )
    return parser.parse_args()