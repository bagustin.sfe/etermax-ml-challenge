# -*- coding: utf-8 -*-
# Imports
import warnings

from preprocessor.preprocessor import Preprocessor, log_tf, log_atf
from parameters import default
from arg_parser.arg_parser import parse_args

from sklearn.metrics import mean_poisson_deviance, accuracy_score
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import lightgbm as lgb

from urllib.parse import urlparse
import logging
import mlflow
import mlflow.lightgbm

mlflow.set_tracking_uri("http://localhost:5050")
warnings.filterwarnings("ignore")


logging.basicConfig(level=logging.WARN)
logger = logging.getLogger(__name__)

def main():
    
    args = parse_args()
    df = pd.read_csv("dataset.csv")
    
    # preprocessing
    preprocessor = Preprocessor()
    preprocessor.droppables = [
                                "user_id",
                                "country", 
                                "country_region", 
                                "device_family", 
                                "os_version",
                                "event_1"
                            ]
    preprocessor.categories = [ "source", "platform" ]
    preprocessor.integers = ["event_2", "event_3"]
    preprocessor.objective = "revenue"
    preprocessor.transform = log_tf

    x, y = preprocessor(df)
    x_train, x_test, y_train, y_test = preprocessor.split(x,y)

    # logging
    
    mlflow.lightgbm.autolog()

    train_data = lgb.Dataset(x_train, label=y_train,
                         categorical_feature=preprocessor.categories)
    test_data = lgb.Dataset(x_test, label=y_test)
    
    with mlflow.start_run():
        # parameters    
        parameters = default.parameters

        parameters["learning_rate"] = args.learning_rate
        parameters["max_depth"] = args.max_depth
        parameters["num_leaves"] = args.num_leaves
        parameters["num_iterations"] = args.num_iterations
        parameters["n_estimators"] = args.n_estimators

        # training
        model = lgb.train(parameters,
                            train_data,
                            valid_sets=test_data,
                            num_boost_round=5000,
                            early_stopping_rounds=100)

        # evaluation
        y_predicted = log_atf(model.predict(x_test))
        y_test = log_atf(y_test.values)
        loss = mean_poisson_deviance(y_test, y_predicted)

        # log metrics
        mlflow.log_params({"learning_rate": args.learning_rate})
        mlflow.log_params({"max_depth": args.max_depth})
        mlflow.log_params({"num_leaves": args.num_leaves})
        mlflow.log_params({"num_iterations": args.num_iterations})
        mlflow.log_params({"n_estimators": args.n_estimators})
        mlflow.log_metrics({"log_loss": loss})
        
        
        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme

        if tracking_url_type_store != "file":

            mlflow.lightgbm.log_model(
                lgb_model=model,
                artifact_path="lightgbm-model",
                registered_model_name="revenue-inference"
            )
        
        else:
            mlflow.lightgbm.log_model(
                lgb_model=model, 
                artifact_path="lightgbm-model",
                registered_model_name="revenue-inference"
            )
        
if __name__ == "__main__":
    main()