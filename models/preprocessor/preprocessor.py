from sklearn.model_selection import train_test_split 
import numpy as np
import pandas as pd

def log_tf(x):
    return np.log(x+1)

def log_atf(x):
    return np.exp(x)-1

class Preprocessor:
    
    def split(self, x, y):
        return train_test_split(x, y, test_size=0.2, random_state=42)

    def __call__(self, df):        
        for c in self.droppables:
            df = df.drop(c, axis="columns")
        for c in self.categories:
            df[c] = df[c].str.split(' ').str[0].str.lower()
            df[c] = df[c].astype("category", copy=False)
        for c in self.integers:
            df[c] = df[c].fillna(0)
            df[c] = df[c].astype("int64", copy=False)
        if self.transform:
            return df.drop(self.objective, axis="columns"), self.transform(df[self.objective])

        return df.drop(self.objective, axis="columns"), df[self.objective]